$(document).ready(function () {

  var app = {
    init: function () {
      var iniciar = function () {
        $('#myModal1').modal('show');
      }
      iniciar();
    }
  }
  app.init();

  // Validar campos
  jQuery.validator.setDefaults({
    errorElement: 'span',
    errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
  });
  $.validator.messages.required = "Este campo es obligatorio";
  $.validator.messages.min = jQuery.validator.format('El campo debe ser mayor que {0}')
  $.validator.messages.max = jQuery.validator.format('El campo debe ser menor que {0}')

  // FIN DATOS DE ENTRADA
  $('#next').on('click', function () {
    event.preventDefault();
    $('#desviacion').html('');
    if ($("#datosEntrada").valid()) {

      var datosEntrada = $('form#datosEntrada').serializeArray();
      var datosEntradaObject = {};
      console.log('datosEntradaObject', datosEntradaObject)
      $.each(datosEntrada,
        function (i, v) {
          datosEntradaObject[v.name] = v.value;
        });
      var data = {
        datosEntrada: datosEntradaObject,
      }
      generarTabla(data);
    }

  });

});

function abrirModal2() {
  $('#myModal1').modal({show:false});
  setTimeout(function() {
    $('#myModal2').modal({show:true});
  }, 500)
}

function generarTabla(data) {
  $('#tablePozos').html('');
  var numMuestras = parseInt(data.datosEntrada.numDatos);
  var clonedTable = '';
  clonedTable = '<div class="">';
  clonedTable += ' <form id="formTableValorCompact">';
  clonedTable += '    <table class="table table-striped table-bordered nowrap" id="tableValorCompac">';
  clonedTable += '      <thead class="thead-dark">';
  clonedTable += '        <tr>';
  clonedTable += '          <th class="text-center" colspan="2">Valor individual del grado de compactacion(%)</th>';
  clonedTable += '        </tr>';
  clonedTable += '      </thead>';
  clonedTable += '      <tbody>';
  clonedTable += '        <tr id="tr">';
  clonedTable += '          <td>Probabilidad</td>';
  clonedTable += '          <td><select class="form-control col-md-12" name="probabilidad" id="probabilidad" required>'
  clonedTable += '            <option value="" selected disabled hidden>Seleccione una opcion</option>';
  clonedTable += '            <option value="60">60%</option>';
  clonedTable += '            <option value="70">70%</option>';
  clonedTable += '            <option value="75">75%</option>';
  clonedTable += '            <option value="80">80%</option>';
  clonedTable += '            <option value="85">85%</option>';
  clonedTable += '            <option value="90">90%</option>';
  clonedTable += '            <option value="95">95%</option>';
  clonedTable += '            <option value="99">99%</option>';
  clonedTable += '          </select></td>';
  clonedTable += '        </tr>';
  for (let index = 0; index < numMuestras; index++) {
    clonedTable += '      <tr id="tr_'+ index +'">';
    clonedTable += '          <td>Valor ' + (index + 1) +'</td>';
    clonedTable += '          <td id="td_' + index +'"><input class="form-control col-md-12" type="number" name="valor_' + index + '" id="valor_' + index + '" min="0" max="200" value="" placeholder="%"></td>';
    clonedTable += '      </tr>';
  }
  clonedTable += '      </tbody>';
  clonedTable += '    </table>';
  clonedTable += '   <div class="row">';
  clonedTable += '      <div class="btn-group btn-group-justified col-sm-12 offset-md-0 col-md-12">';
  clonedTable += '        <button id="calculate" class="btn btn-primary btn-sm">Calcular</button>';
  clonedTable += '      </div>';
  clonedTable += '   </div>';
  clonedTable += '  </form>'
  clonedTable += '</div>';
  clonedTable += '</br>';
  $('#tablePozos').append(clonedTable);

  $('#calculate').on('click', function () {
    event.preventDefault();
    if ($("#formTableValorCompact").valid()) {
      var datosEntrada = $('form#formTableValorCompact').serializeArray();
      calcular(datosEntrada, numMuestras);
      var elmnt = document.getElementById("desviacion");
      elmnt.scrollIntoView();
    }
  });

}

function calcular(data, numMuestras) {
  var valores = [];
  $.each(data,
    function (i, v) {
      if(i !== 0) {
        valores.push(v.value)
      }
  });
  valores = valores.map(i=>Number(i));
  console.log('valores', valores);

  var desviacion = math.std([valores]); // Calcular desviacion
  desviacion = desviacion.toFixed(3);
  // console.log('Desviacion', desviacion);

  // Calcular promedio
  let sum = valores.reduce((previous, current) => current += previous); 
  let promedio = sum / valores.length;
  promedio = promedio.toFixed(3);
  // console.log('promedio', promedio);

  var matriz = [
    ['p', 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
    [60,  0.199, 0.160, 0.138, 0.121, 0.109, 0.100, 0.093, 0.087, 0.083, 0.078, 0.075, 0.072, 0.069, 0.067],
    [70,  0.423, 0.338, 0.292, 0.254, 0.228, 0.209, 0.194, 0.182, 0.172, 0.163, 0.156, 0.149, 0.144, 0.139],
    [75,  0.558, 0.443, 0.382, 0.331, 0.297, 0.271, 0.251, 0.235, 0.222, 0.211, 0.201, 0.193, 0.185, 0.179],
    [80,  0.717, 0.566, 0.489, 0.421, 0.375, 0.342, 0.317, 0.296, 0.279, 0.265, 0.253, 0.242, 0.233, 0.224],
    [85,  0.925, 0.724, 0.625, 0.532, 0.472, 0.429, 0.396, 0.369, 0.348, 0.330, 0.314, 0.300, 0.289, 0.278],
    [90,  1.229, 0.949, 0.819, 0.686, 0.603, 0.544, 0.500, 0.466, 0.437, 0.414, 0.394, 0.376, 0.361, 0.347],
    [95,  1.813, 1.365, 1.177, 0.953, 0.823, 0.734, 0.670, 0.620, 0.580, 0.546, 0.518, 0.494, 0.473, 0.455],
    [99,  3.657, 2.588, 2.270, 1.676, 1.374, 1.188, 1.060, 0.965, 0.892, 0.833, 0.785, 0.740, 0.780, 0.678],
  ];

  // Calcular K(p)
  var filaN = matriz[0].indexOf(numMuestras);
  var columnaProbabilidad = -1;
  for (let index = 0; index < matriz.length; index++) {
    const element = matriz[index];
    if(columnaProbabilidad === -1) {
      columnaProbabilidad = (element[0] === parseInt(data[0].value)) ? index : -1;
    }
  }
  var k = matriz[columnaProbabilidad][filaN];
  // console.log('k', k);

  //Calcular GCi en porcentaje
  var gci = promedio - (k * desviacion);
  gci = gci.toFixed(3);
  // console.log('gci', gci);

  //Calcular si se acepta o no
  var aceptar = (gci > 95) ? true : false;

  var info = '';
  info += '<div class="alert ' + (aceptar ? 'alert-success' : 'alert-danger') +'" role="alert">';
  info += ' <h4 class="alert-heading">' + (aceptar ? 'SE APRUEBA LOTE' : 'SE RECHAZA LOTE') + '!</h4><hr>'
  info += ' <p><strong>promedio:</strong> ' + promedio + '</p><hr>'
  info += ' <p><strong>Desviacion:</strong> ' + desviacion + '</p><hr>'
  info += ' <p><strong>K(p):</strong> ' + k + '</p><hr>'
  info += ' <p><strong>GCI(90):</strong> ' + gci + '</p><hr>'
  info += '</div>';
  $('#desviacion').html('');
  $('#desviacion').append(info);
}
